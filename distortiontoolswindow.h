#ifndef DISTORTIONTOOLSWINDOW_H
#define DISTORTIONTOOLSWINDOW_H

#include <QMainWindow>
#include <QSettings>
#include <opencv2/opencv.hpp>
#include <opencv2/video.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>

#define INTRINSICS_SIZE 9
#define DISTORTION_SIZE 4


typedef struct cameraProfile {
    char name[64];
    double intrinsics[INTRINSICS_SIZE];
    double distortionCoeffs[DISTORTION_SIZE];
    float captureSize[2];
    int split; // 0 for fullscreen, 1 for left, 2 for right
    float reprojectionError;
} CameraProfile;

typedef enum uiMode
{
    UI_MODE_CAMERA_CALIBRATION,
    UI_MODE_LENS_DISTORTION,
    UI_MODE_MESH_EDIT
} UiMode;

class ProfileSerialiser {
public:
    QString toString(std::vector<CameraProfile>*);
    bool fromString(std::vector<CameraProfile>*,QString s);
};

namespace Ui {
class DistortionToolsWindow;
}

class DistortionToolsWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DistortionToolsWindow(QWidget *parent = 0);
    ~DistortionToolsWindow();
public slots:
    void captureNextImage();
    void lensCaptureNextImage();
    void resetImageCapture();
    void lensResetImageCapture();
    void calculateDistortion();
    void applyManual();
    void addCameraProfile();
    void savePNG();
    void saveData();
    void showGridOverlay(int i);
    void update();
    void lensCalculateDistortion();
    void lensApplyManual();
    void setUIDirty();


private:
    QSettings* settings;
    Ui::DistortionToolsWindow *ui;
    bool uiDirty;
    UiMode uiMode;
    ProfileSerialiser ps;
    std::vector<CameraProfile> cameraProfiles;
    cv::VideoCapture* cap;
    cv::Mat origImage;
    cv::Mat image;
    cv::Mat undistortedImage;
    cv::Mat intrinsic;
    cv::Mat distCoeffs;
    cv::Mat lensIntrinsic;
    cv::Mat lensDistCoeffs;
    cv::Mat lensUndistortedImage;

    std::vector<std::vector<cv::Point3f>> objectPoints;
    std::vector<std::vector<cv::Point2f>> imagePoints;
    std::vector<std::vector<cv::Point3f>> lensObjectPoints;
    std::vector<std::vector<cv::Point2f>> lensImagePoints;
    uint8_t staticGridSize;
    std::vector<cv::Point3f> staticGridPoints;
    cv::Size boardSize;
    std::vector<cv::Point3f> fixedChessboardPoints;
    cv::Size lensBoardSize;
    std::vector<cv::Point3f> lensFixedChessboardPoints;

    bool distortionCalculated;
    bool lensDistortionCalculated;

    uint16_t imagesCaptured;
    uint16_t lensImagesCaptured;
};

#endif // DISTORTIONTOOLSWINDOW_H
