#ifndef DISTORTIONWIDGET_H
#define DISTORTIONWIDGET_H
#include <QWidget>
#include <QImage>
#include <QMouseEvent>

class DistortionWidget : public QWidget
{
    Q_OBJECT
    private:
        std::vector<QPointF> gridPoints;
        QPointF lineA;
        QPointF lineB;
        uint16_t gridX;
        uint16_t gridY;
        float imageWidth;
        float imageHeight;
    public:
        bool showGridOverlay;
        bool showVideo;
        void clearGridPoints();
        void setGridSize(uint8_t d);
        void addGridPoint(float px, float py);
        DistortionWidget(QWidget* _parent);
        QImage* videoImage;
        void paintEvent(QPaintEvent*);
        void mousePressEvent(QMouseEvent* e);
        void update();
        void setImage(uint8_t* data,uint32_t width, uint32_t height,uint32_t channels);
};

#endif // DISTORTIONWIDGET_H
